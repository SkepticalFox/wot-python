from idaapi import *
from idautils import *
from idc import *

import simplejson as json
import os

json_pyapi_func_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'json', 'WoT_PyAPI_FUNC.json')
json_pyapi_data_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'json', 'WoT_PyAPI_DATA.json')

with open(json_pyapi_func_path, 'r') as f:
    wot_pyapi_func_json = json.load(f)

with open(json_pyapi_data_path, 'r') as f:
    wot_pyapi_data_json = json.load(f)

#WoT = '09.14.8318'
#WoT = '09.15.9592'
#WoT = '09.15.10073'
#WoT = '09.15.10729'
WoT = '09.16.11080'

for (key, value) in wot_pyapi_data_json['PyAPI_DATA'].items():
    if key == '__null__':
        continue
    if not value['func_offsets']:
        continue
    if value['offsets'].get(WoT) is not None:
        continue
    for func_offset in value['func_offsets']:
        name, offset = func_offset.split('+')
        if wot_pyapi_func_json['PyAPI_FUNC'].get(name) is None:
            continue
        func_addr = wot_pyapi_func_json['PyAPI_FUNC'][name]['offsets'].get(WoT)
        if func_addr is None:
            continue
        offset = int(offset, 16)
        abs_offset = int(func_addr, 16) + offset
        for j in XrefsFrom(abs_offset):
            continue
        value['offsets'][WoT] = hex(int(j.to))
        break

with open(json_pyapi_data_path, 'w') as f:
    json.dump(wot_pyapi_data_json,
              f,
              sort_keys=True,
              indent='\t',
              separators=(',', ' : '))
