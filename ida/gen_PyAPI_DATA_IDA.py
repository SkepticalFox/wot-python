from idaapi import *
from idautils import *
from idc import *

import simplejson as json
import os

json_pyapi_func_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'json', 'WoT_PyAPI_FUNC.json')
json_pyapi_data_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'json', 'WoT_PyAPI_DATA.json')

with open(json_pyapi_func_path, 'r') as f:
    wot_pyapi_func_json = json.load(f)

with open(json_pyapi_data_path, 'r') as f:
    wot_pyapi_data_json = json.load(f)

#WoT = '09.14.8318'
#WoT = '09.15.9592'
#WoT = '09.15.10073'
#WoT = '09.15.10729'
WoT = '09.16.11080'

for (key, value) in wot_pyapi_data_json['PyAPI_DATA'].items():
    if key == '__null__':
        continue
    if value['offsets'].get(WoT) is None:
        continue
    if value['func_offsets']:
        continue
    i = 0
    addr = int(value['offsets'][WoT], 16)
    #value['func_offsets'] = []
    for j in XrefsTo(addr):
        func_name = get_func_name(j.frm)
        if func_name is None:
            continue
        if func_name.startswith('sub_'):
            continue
        if value['func_offsets'] and value['func_offsets'][0].split('+')[0] == func_name:
            continue
        if wot_pyapi_func_json['PyAPI_FUNC'].get(func_name) is None:
            continue
        if i >= 2:
            break
        i += 1
        offset = hex(int(j.frm - get_func(j.frm).startEA))
        value['func_offsets'].append('%s+%s' % (func_name, offset))

with open(json_pyapi_data_path, 'w') as f:
    json.dump(wot_pyapi_data_json,
              f,
              sort_keys=True,
              indent='\t',
              separators=(',', ' : '))
