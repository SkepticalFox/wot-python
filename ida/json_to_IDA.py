from idaapi import *
from idautils import *
from idc import *

import sigutils

reload(sigutils)

import simplejson as json
import os

json_pyapi_func_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'json', 'WoT_PyAPI_FUNC.json')
json_pyapi_data_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'json', 'WoT_PyAPI_DATA.json')

with open(json_pyapi_func_path, 'r') as f:
    wot_pyapi_func_json = json.load(f)

with open(json_pyapi_data_path, 'r') as f:
    wot_pyapi_data_json = json.load(f)

#WoT = '09.14.8318'
#WoT = '09.15.9592'
#WoT = '09.15.10073'
#WoT = '09.15.10729'
WoT = '09.16.11080'

for (key, value) in wot_pyapi_data_json['PyAPI_DATA'].items():
    if key == '__null__':
        continue
    if value['offsets'].get(WoT) is not None:
        off = int(value['offsets'][WoT], 16)
        MakeNameEx(off, str(key), SN_NOWARN)

for (key, value) in wot_pyapi_func_json['PyAPI_FUNC'].items():
    func = None
    if key == '__null__':
        continue
    if value['offsets'].get(WoT) is not None:
        func = get_func(int(value['offsets'][WoT], 16))
        if value['signatures'] is None and func is not None:
            sig = sigutils.getSig(func.startEA)
            if sig is not None:
                value['signatures'] = [{'versions':WoT, 'signature':sig}]
    elif value['signatures'] is not None:
        for _sig in value['signatures']:
            if WoT in _sig['versions']:
                func = get_func(FindBinary(0, SEARCH_DOWN, str(_sig['signature'])))
                break
    if func is not None:
        MakeNameEx(func.startEA, str(key), SN_NOWARN)
        if value['offsets'].get(WoT) is None:
            value['offsets'][WoT] = hex(int(func.startEA))

with open(json_pyapi_func_path, 'w') as f:
    json.dump(wot_pyapi_func_json,
              f,
              sort_keys=True,
              indent='\t',
              separators=(',', ' : '))
