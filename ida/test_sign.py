import sigutils
import simplejson as json
import os

#WoT = '09.14.8318'
#WoT = '09.15.9592'
#WoT = '09.15.10073'
#WoT = '09.15.10729'
WoT = '09.16.11080'

json_pyapi_func_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'json', 'WoT_PyAPI_FUNC.json')

with open(json_pyapi_func_path, 'r') as f:
    wot_pyapi_func_json = json.load(f)

for (key, value) in wot_pyapi_func_json['PyAPI_FUNC'].items():
    if value['signatures'] is None:
        continue
    for _sig in value['signatures']:
        if WoT in _sig['versions']:
            sig = _sig['signature']
            if sig is not None:
                assert sigutils.IsGoodSig(sig), (key, sig)
