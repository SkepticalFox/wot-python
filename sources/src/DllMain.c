#include <Windows.h>
#include "DllMain.h"
#include "../pythonwrapper/pythonWrapper.h"

BOOL WINAPI DllMain(IN HINSTANCE hDllHandle, IN DWORD nReason, IN LPVOID Reserved)
{
    switch ( nReason ) 
    {
    case DLL_PROCESS_ATTACH:
        pythonWrapperInit();
        break;
    case DLL_PROCESS_DETACH:
        break;
    }

    return TRUE;
}
