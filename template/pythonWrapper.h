#pragma once

#define Py_BUILD_CORE 1

#include "Python.h"
#include "Python-ast.h"
#include "node.h"

void pythonWrapperInit(void);

%s