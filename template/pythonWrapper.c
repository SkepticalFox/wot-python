#include "pythonWrapper.h"
#include "pythonWrapperHelper.h"

%s

void pythonWrapperInit(void)
{
    DWORD startpos;
    DWORD curpos;
    DWORD endpos;
    TCHAR lpFilename[2048];

    GetModuleFileName(NULL, lpFilename, 2048);
    startpos = (DWORD)GetModuleHandleA(lpFilename);
    endpos = (DWORD)GetModuleSize(lpFilename);
    curpos = startpos;

%s
}
