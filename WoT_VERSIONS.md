### WoT versions ###

in version.xml    | in ?        | files
----------------- | ----------- | ----------
?                 | 09.8.6054   | ?
?                 | 09.9.6394   | ?
?                 | 09.10.6769  | ?
v.0.9.14 #132     | 09.14.7905  | ?
v.0.9.14.1 #143   | 09.14.8133  | [9E.8133](http://dl-wot-gc.wargaming.net/ru/patches/9.E_3_KGuJCEp4m/wot_9E.8133_9E.8014_client.patch.torrent)
v.0.9.14.1 #144   | 09.14.8135  | [9E.8135](http://dl-wot-gc.wargaming.net/ru/patches/9.E_3_KGuJCEp4m/wot_9E.8133_9E.8014_client.patch.torrent)
v.0.9.14.1 #153   | 09.14.8318  | [9.14.8318](http://forum.worldoftanks.ru/index.php?/topic/1754664-/page__pid__42903666#entry42903666)
v.0.9.15.1 #183   | 09.15.9592  | [9.15.9592](hhttp://dl-wot-gc.wargaming.net/ru/patches/9.15.1_FoC73tGc56/wot_9.15.9592_9.15.9213_client.wgpkg.torrent)
v.0.9.15.1.1 #194 | 09.15.10073 | [9.15.10073](http://dl-wot-gc.wargaming.net/ru/patches/9.15.1_1_FoC73tGc56/wot_9.15.10073_9.15.9592_client.wgpkg)
v.0.9.15.2 #213   | 09.15.10729 | [9.15.10729](http://dl-wot-ak.wargaming.net/ru/patches/9.15.1_3_VP5zfkr8Ua/wot_9.15.10729_9.15.10306_client.wgpkg)
v.0.9.16 #227     | 09.16.11080 | [9.16.11080](http://www.koreanrandom.com/forum/topic/33507-/)
