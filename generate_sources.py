import json

with open('.\\template\\pythonWrapper.c', 'r') as f:
    pythonwrapper_c = f.read()

with open('.\\template\\pythonWrapper.h', 'r') as f:
    pythonwrapper_h = f.read()

WoT = '09.15.10073'

sourceDeclaration = ''
sourceInitialization = ''


def genHRecord(name, func):
    s = '// %s\n' % name
    s += 'typedef PyAPI_FUNC({0}) wrp_{1}_typedef({2});\n'.format(
        func['return_type'], name, func['arguments'])
    s += 'extern wrp_{0}_typedef* wrp_{0}_func;\n'.format(name)
    s += '#define {0} wrp_{0}_func\n\n'.format(name)
    return s


def ProcessSignature(pattern):
    s_spi = pattern.split(' ')
    b_pat = ''
    b_mask = ''
    for i in s_spi:
        if '?' in i:
            b_pat += '\\x00'
            b_mask += '?'
        else:
            b_pat += '\\x' + i
            b_mask += 'x'
    return (b_pat, b_mask)


def genCppRecord(name, func):
    global sourceDeclaration, sourceInitialization
    sourceDeclaration += 'wrp_{0}_typedef* wrp_{0}_func;\n'.format(name)
    sign = ProcessSignature(func['signature'])
    sourceInitialization = "    wrp_{0}_func = (wrp_{0}_typedef*)(FindFunction(startpos, endpos, &curpos, \"{1}\",\"{2}\"));\n".format(name, sign[0], sign[1])


functions = None
with open('.\\json\\WoT_PyAPI_FUNC.json', 'r') as fr:
    functions = json.load(fr)['PyAPI_FUNC']

with open('.\\sources\\pythonwrapper\\pythonWrapper.h', 'w') as f:
    count = 0
    _str = ''
    for key, value in functions.items():
        if value['signature'] is None:
            continue
        count += 1
        _str += genHRecord(key, value)
    f.write(pythonwrapper_h % _str)
    print('Generated %s functions.' % count)

with open('.\\sources\\pythonwrapper\\pythonWrapper.c', 'w') as f:
    for key, value in functions.items():
        if value['signature'] is None:
            continue
        genCppRecord(key, value)
    f.write(pythonwrapper_c % (sourceDeclaration, sourceInitialization))
